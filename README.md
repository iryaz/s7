Задача: Нужно реализовать два Java Spring Boot приложения.


Технические требования: 

1. Версия Java 8+, Spring Boot 2.1+. Без Spring Cloud.

2. Собираться должно с помощью Maven или Gradle. Нужен хороший README с командами сборки и запуска приложений. Если приложения не собираются или не запускаются, то тестовое автоматически считается не пройденным.

3. Версия Kafka 2.1+. Для работы с Kafka необходимо использовать Spring Kafka (можно дополнительно добавить Kafka Streams, где это применимо, будет как плюс).

4. Код должен быть читаемым, "чистым" и поддерживаемым, должны использоваться лучшие практики для написания кода (DRY, SOLID, например).

 

Бизнес-требования:

1. Первый сервис должен иметь HTTP PUT эндпойнт, который должен иметь вид test/{id}, где id - идентификатор сущности (тип - UUID v4). В тело запроса должен передаваться следующий JSON-объект: 

{

  "name": "name",

  "sum": 1,

  "multiplier": 2

}

, где name - строковый тип, sum - целочисленный тип, multiplier - целочисленный тип

2. Этот объект должен один в один передаваться в Apache Kafka

3. Второе приложение должно получать этот объект из Apache Kafka и приводить его к виду:

{

  "id": "225eeda1-b71e-4836-b009-c532873d9021",

  "name": "name",

  "multipliedSum": "2"

}

, где: id - полученный из пункта 1 бизнес-требований, name - имя из объекта полученного из Kafka, multipliedSum - произведение sum и multiplier в виде строки

4. Записывать полученное сообщение в лог, используя SLF4J. Уровень логирования - DEBUG.



Stack:
  - java 8
  - Kafka 2.1+
  - http
  - maven

App run(for each service):
  1. install and configure Apache Kafka(once)
  2. cd to folder of the project(democonsumer-service, demoproducer-service)
  3. mvn package
  4. cd to target
  5. java -jar *appName*.jar
  
  
API:
  curl -X PUT http://127.0.0.1:8080/test/041197c3-7d91-4066-a349-1c62ed24355b -d '{"name": "name", "sum": 1, "multiplier": 2}' -H "Content-Type:application/json"

