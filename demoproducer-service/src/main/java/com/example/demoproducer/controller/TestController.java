package com.example.demoproducer.controller;

import com.example.demoproducer.Kafka.MessageProducer;
import com.example.demoproducer.model.MyProducerData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@Slf4j
@Validated
@RestController
public class TestController {

    @Autowired
    MessageProducer messageProducer;

    public static final String UUID_PATTERN = "^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$";

    @PutMapping("/test/{id}")
    public void index(@PathVariable("id") @Pattern(regexp = UUID_PATTERN, message = "UUID is not valid") String id,
                      @Valid @RequestBody MyProducerData data) {
        log.info("PUT msgId - {}, data - {}", id, data);
        messageProducer.sendMessage(id, data);
    }
}
