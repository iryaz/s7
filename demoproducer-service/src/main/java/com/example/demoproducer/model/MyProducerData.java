package com.example.demoproducer.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "sum",
        "multiplier"
})
public class MyProducerData implements Serializable {

    @NotNull
    @JsonProperty("name")
    private String name;

    @NotNull
    @JsonProperty("sum")
    private Integer sum;

    @NotNull
    @JsonProperty("multiplier")
    private Integer multiplier;

    private final static long serialVersionUID = -2182422734496402801L;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String type) {
        this.name = type;
    }

    @JsonProperty("sum")
    public Integer getSum() {
        return sum;
    }

    @JsonProperty("sum")
    public void setSum(Integer sum) {
        this.sum = sum;
    }

    @JsonProperty("multiplier")
    public Integer getMultiplier() {
        return multiplier;
    }

    @JsonProperty("multiplier")
    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}