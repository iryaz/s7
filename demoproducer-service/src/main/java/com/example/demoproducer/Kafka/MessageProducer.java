package com.example.demoproducer.Kafka;

import com.example.demoproducer.model.MyProducerData;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Slf4j
@NoArgsConstructor
@Component
public class MessageProducer {

    @Autowired
    private KafkaTemplate<String, MyProducerData> kafkaTemplate;

    @Value(value = "${kafka.topic.name}")
    private String topicName;


    public void sendMessage(String uuid, MyProducerData data) {

        ListenableFuture<SendResult<String, MyProducerData>> future = kafkaTemplate.send(topicName, uuid, data);

        future.addCallback(new ListenableFutureCallback<SendResult<String, MyProducerData>>() {
            @Override
            public void onFailure(Throwable throwable) {
                log.error("Unable to send msg with id {} dut to: {}", data, throwable.getMessage());
            }

            @Override
            public void onSuccess(SendResult<String, MyProducerData> stringDataSendResult) {
                log.info("Send msg {} with offset = {}", data, stringDataSendResult.getRecordMetadata().offset());
            }
        });
    }
}
