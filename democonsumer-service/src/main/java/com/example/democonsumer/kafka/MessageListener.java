package com.example.democonsumer.kafka;

import com.example.democonsumer.model.ConsumerData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

@Slf4j
public class MessageListener {

    @KafkaListener(topics = "${kafka.topic.name}", containerFactory = "kafkaListenerContainerFactory")
    public void listener(@Payload ConsumerData data, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String messageKey) {
        data.setId(messageKey);
        log.debug("--------RECEIVED MSG--------------: {}", data);
    }
}
