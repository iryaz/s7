package com.example.democonsumer.model;

import com.example.democonsumer.serialize.ConsumerDataDeserializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "multipliedSum",
        "id"
})
@JsonDeserialize(using = ConsumerDataDeserializer.class)
public class ConsumerData implements Serializable {

    @JsonProperty("name")
    private String name;

    @JsonProperty("multipliedSum")
    private String multipliedSum;

    @JsonProperty("id")
    private String id;

    private final static long serialVersionUID = 6467717984886707689L;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("multipliedSum")
    public String getMultipliedSum() {
        return multipliedSum;
    }

    @JsonProperty("multipliedSum")
    public void setMultipliedSum(String multipliedSum) {
        this.multipliedSum = multipliedSum;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
