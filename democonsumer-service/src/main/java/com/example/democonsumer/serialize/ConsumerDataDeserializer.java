package com.example.democonsumer.serialize;

import com.example.democonsumer.model.ConsumerData;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class ConsumerDataDeserializer extends JsonDeserializer<ConsumerData> {
    private static final String RECIEIVED_SUM = "sum";
    private static final String RECIEIVED_MULTIPLIER = "multiplier";
    private static final String RECIEIVED_NAME = "name";

    @Override
    public ConsumerData deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectCodec oc = jp.getCodec();
        JsonNode node = oc.readTree(jp);
        final Integer sum = node.get(RECIEIVED_SUM).asInt();
        final String name = node.get(RECIEIVED_NAME).asText();
        final Integer multiplier = node.get(RECIEIVED_MULTIPLIER).asInt();
        ConsumerData data = new ConsumerData();
        data.setName(name);
        data.setMultipliedSum(Integer.toString(sum * multiplier));
        return data;
    }
}
